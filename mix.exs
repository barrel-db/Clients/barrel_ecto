defmodule BarrelEcto.MixProject do
  use Mix.Project

  @version "0.1.0"

  def project do
    [
      app: :barrel_ecto,
      version: @version,
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto, "~> 2.1.0"},
      {:barrex, git: "https://gitlab.com/barrel-db/Clients/barrel_ex.git", branch: "master"},
    ]
  end
end
